angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('CameraCtrl', function($scope, $stateParams, $cordovaCamera) {
    $scope.fotoBase64 = "";

    $scope.baterFoto = function(){

      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 200,
        targetHeight: 200,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation:true
      }

      $cordovaCamera.getPicture(options).then(function(imageData) {
        //file/dsjashdasd.jpeg
        $scope.fotoBase64 = "data:image/png;base64, " + imageData;
      }, function(err) {
        // error
      });
    }

})

.controller('AlertaCtrl', function($scope, $stateParams, $cordovaToast, $ionicPlatform) {
  
  $scope.mostrarAlerta = function(){
    $cordovaToast.show('Alerta!!', 'long', 'center')
    .then(function(success) {
      // success
    }, function (error) {
      // error
    });
  }

})

.controller('LocalizacaoCtrl', function($scope, $stateParams, $cordovaGeolocation) {
  $scope.latLong = {};

  $scope.minhaLatLong = function(){
    $cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy: false})
    .then(function (position) {
      $scope.latLong = {lat: position.coords.latitude, long: position.coords.longitude}
    }, function(err) {
      // error
    });
  }

})

.controller('CodigoBarraCtrl', function($scope, $stateParams) {

})

.controller('FormularioCtrl', function($scope, $stateParams, $cordovaSQLite, $ionicPlatform) {

    var db;

    $ionicPlatform.ready(function () {
      
      if (window.cordova && window.cordova.platformId != 'browser') {
        db = $cordovaSQLite.openDB({name: "aula.db", background: 1, location: 'default'}); //device
      } else {
        db = window.openDatabase("aula.db", '1', 'Aula', 1024 * 1024 * 100);
      }

      //$cordovaSQLite.execute(db, "DROP TABLE IF EXISTS contato");

      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS contato (id INTEGER PRIMARY KEY AUTOINCREMENT, nome VARCHAR(255), telefone VARCHAR(255))").then(function(){

      }, function(err){
        console.info(err)
      });

    });

    function insertContato(nome, telefone){

      $cordovaSQLite.execute(db, "INSERT INTO contato (nome, telefone) VALUES (?,?)", [nome, telefone]).then(function(result){
        console.info(result);
      }, function(err){

      });

    }

    $scope.selectContato = function(){

      $cordovaSQLite.execute(db, "SELECT * FROM contato").then(function(result){
        console.info(result.rows);
        $scope.listaContato = result.rows;
      }, function(err){

      });

    }

    $scope.contato = {nome:"", telefone:""};

    $scope.salvar = function(){ 
      insertContato($scope.contato.nome, $scope.contato.telefone); 
    }
})

.controller('ApiCtrl', function($scope, $stateParams, $http, BASE_URL, $ionicLoading, $ionicModal) {

  $scope.produtos = [];
  $scope.modal = null;

  $scope.carregar = function(){

    $ionicLoading.show({
      template: 'Carregando...'
    });

    $http.get(BASE_URL+"/products").then(function(resp){
      $ionicLoading.hide();
      $scope.produtos = resp.data;
    }, function(err){
      console.error(err)
    })
  }

  $scope.detalhes = function(produto){
    console.info(produto);

    $ionicModal.fromTemplateUrl('templates/modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal; 
      $http.get(BASE_URL+"/product/"+produto._id).then(function(resp){
        $scope.produto = resp.data;
        modal.show();
      });
    });
  }

  $scope.fecharDetalhe = function(){
    $scope.modal.hide();
  }

  $scope.novoProduto = function(){
    $ionicModal.fromTemplateUrl('templates/modalForm.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal; 
      modal.show();
    });
  }

  $scope.salvarProduto = function(produto){
    produto['prices'] = [{unit: "cx", outPrice: produto.price, fromAmount: 1}]
    console.info(produto)

    $ionicLoading.show({
      template: 'Salvando produto...'
    });

    $http.post(BASE_URL+"/product", produto).then(function(resp){
      $ionicLoading.hide();
      $scope.carregar();
      $scope.modal.hide();
    })
  }


})








