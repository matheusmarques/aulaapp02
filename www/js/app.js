// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.camera', {
    url: '/camera',
    views: {
      'menuContent': {
        templateUrl: 'templates/camera.html',
        controller: "CameraCtrl"
      }
    }
  })

  .state('app.alerta', {
      url: '/alerta',
      views: {
        'menuContent': {
          templateUrl: 'templates/alerta.html',
          controller: "AlertaCtrl"
        }
      }
    })
    .state('app.localizacao', {
      url: '/localizacao',
      views: {
        'menuContent': {
          templateUrl: 'templates/localizacao.html',
          controller: 'LocalizacaoCtrl'
        }
      }
    })

  .state('app.codigoBarra', {
    url: '/codigoBarra',
    views: {
      'menuContent': {
        templateUrl: 'templates/codigoBarra.html',
        controller: 'CodigoBarraCtrl'
      }
    }
  })

  .state('app.formulario', {
    url: '/formulario',
    views: {
      'menuContent': {
        templateUrl: 'templates/formulario.html',
        controller: 'FormularioCtrl'
      }
    }
  })

  .state('app.api', {
    url: '/api',
    views: {
      'menuContent': {
        templateUrl: 'templates/api.html',
        controller: 'ApiCtrl'
      }
    }
  })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/camera');
})

.constant('BASE_URL', "http://localhost:8100/apiRest")
